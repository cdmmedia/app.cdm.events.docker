# Testing removal of autobuild from depricated docker repo

FROM nginx:alpine

LABEL maintainer = "Matt Capitao <matt.capitao@cdmmedia.com>"

COPY code /usr/share/nginx/html/

EXPOSE 80