<?php
session_start();

$action='get';

if(isset($_GET['e'])){
	$_SESSION['active_eventId']=$_GET['e'];
	$_SESSION['active_eventName']=$_GET['n'];
	$action='set';
}

$response['id']=$_SESSION['active_eventId'];
$response['name']=$_SESSION['active_eventName'];
$response['action']=$action;

header('Content-Type: application/json');
echo json_encode($response);
?>