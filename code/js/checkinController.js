onsiteApp.filter('customFilterToSearchByName', function() {
  return function(input, search) {
    if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
      var actual = ('' + value.attendee_name).toLowerCase();
      if (actual.indexOf(expected) !== -1) {
        result[key] = value;
      }
    });
    return result;
  }
});

/**************************** BEGIN: checkinApp Controller ***********************************/
onsiteApp.controller('checkinController', function ($scope, $http, $window, activeEventService) {

	/***************** Function to get all event attendees *****************************/
	$scope.getAllEventAttendees = function(id) {
		$http.get("https://admin.dovetail.events/webservice/users/delegatesList?event_id="+id+"&type_id=2")
			.then(function(response) {		
				$scope.eventAttendees = response.data.AttendeesList.data;
				$scope.getAllSessions(id);
			});
	};
	
	/***************** Function to get all sessions by event *****************************/
	$scope.getAllSessions = function(id) {
		$http.get("https://admin.dovetail.events/webservice/dovetailsession/sessionsList?event_id="+id)
			.then(function(response) {	
				$scope.sessions = response.data;
				$scope.attendeeSessionData = buildAttendeeSessionIndexObj();
				$scope.attendees = $scope.attendeeSessionData;
			});
	};
	
	function getAttendeeSessionData(attendeeId) {
		var attendeeSessionData = [];
		angular.forEach($scope.sessions, function(value, key) {
			angular.forEach(value.session_attendees, function(v, k) {
				if(attendeeId === v.id) {
					attendeeSessionData.push(value.session_id);
				}
			});
		});
		return attendeeSessionData;
	}
	
	function buildAttendeeSessionIndexObj() {
		var attendeeIndex = [];
		angular.forEach($scope.eventAttendees, function(value, key) {
			var attendeeId = value.attendee_id;
			var view_state='active'; 
			if(value.checked_in==1){view_state='faded';}
			var sessionData = getAttendeeSessionData(attendeeId);
			var attendeeDetails = {attendee_id: attendeeId, event_attendee_id: value.event_attendee_id, attendee_designation: value.attendee_designation, attendee_name: value.attendee_name, attendee_company_name: value.attendee_company_name, attendee_photo_thumb: value.attendee_photo_thumb, event_check_in: value.checked_in, checked_in: value.checked_in, att_session: sessionData, tile_view_state: view_state};
			var attendee = {}; 
			attendee[attendeeId] = attendeeDetails;
			attendeeIndex.push(attendee);
		});
		
		return attendeeIndex;
	}
	
	/***************** Function to check-in attendees to a session *****************************/
	$scope.attendeeSessionCheckin = function(e, profile, session) {
		var elem = angular.element(e.currentTarget);
		if($scope.view === 'checkin') {
			if(session) {
				var dataObj = {
					event_id:$scope.eventData.id,
					attendee_id : profile.attendee_id,
					session_id: session.session_id
				}; 
				var url = 'https://admin.dovetail.events/webservice/dovetailsession/attendeeSessionCheckin';

				var config = { headers : { 'Content-Type': 'application/json'}};
				var res = $http.post(url, dataObj, config);

				res.then(function(response){
					profile.tile_view_state='faded';
					profile.att_session.push(session.session_id);
					profile.checked_in = "1";
					
				},function(response){
					console.log( "failure message:" + JSON.stringify({data: response}));
				});
			} else {
				var dataObj2 = {
					event_attendee_id: profile.event_attendee_id,
					checked_in:"1"
				}; 
				var url2 = 'https://admin.dovetail.events/webservice/event_attendees/checkin';

				var config2 = { headers : { 'Content-Type': 'application/json'}};
				var res2 = $http.post(url2, dataObj2, config2);

				res2.then(function(response){
					profile.tile_view_state='faded';
					profile.checked_in = "1";
					profile.event_check_in = "1";
				},function(response){
					console.log( "failure message:" + JSON.stringify({data: response}));
				});
			}
		} 
	};
	
	/***************** Function to set app view *****************************/
	$scope.toggleVal = true;
	$scope.view = 'checkin'
	
	$scope.setView = function(session) {
		$scope.view = $scope.toggleVal ? 'checkin' : 'session';
		$scope.getAttendeesBySession(session);
	};
	
	/***************** Function to get event attendees by session *****************************/
	$scope.getAttendeesBySession = function(session){
		
		$scope.selectedSession=session;
		
		angular.forEach($scope.attendeeSessionData, function(value, key) {

			angular.forEach(value, function(v, k) {
				angular.element(document.getElementById(v.attendee_id)).removeClass(v.tile_view_state);
				switch($scope.view){
					case'checkin':
						v.tile_view_state='active';
						
						if(session/*typeof session !== "undefined"*/){
							if(v.att_session){
							if(v.att_session.indexOf(session.session_id) >= 0){
								v.tile_view_state='faded';
							}}
						}else if(v.checked_in==1){
							{v.tile_view_state='faded';}
						}
					break;
				
					case 'session' :
						v.tile_view_state='hidden';
						if(session/*typeof session !== "undefined"*/){
							if(v.att_session){
							if(v.att_session.indexOf(session.session_id) >= 0){
								v.tile_view_state='active';
							}}
						}else if(v.checked_in==1){
							v.tile_view_state='active';
							
						}
					break;
				}
				angular.element(document.getElementById(v.attendee_id)).addClass(v.tile_view_state);
			});
		});
		
		$scope.attendees = $scope.attendeeSessionData;
		
		
	};

	$scope.pusherInit =function(eventId){
		
		if(eventId===null){
			alert('No active event is selected. Real time updates are disabled.');
			console.log('Pusher connection could not be established because the event id is not set.');
			return;
		}
		var pusher = new Pusher('9d65b7684021a926f7be', {cluster: 'us2', encrypted: true});
		var subscribeTo = 'eventId-'+eventId;
		var channel = pusher.subscribe(subscribeTo);

		channel.bind('check-in', function(data) {
			var attendeeElem = angular.element(document.getElementById(data.attendee_id));
			var attendeeObj = attendeeElem.scope().attendee[data.attendee_id];
			console.log(data);

			if(data.checkinType==='event'){
				attendeeObj.checked_in = data.checked_in;
				attendeeObj.event_check_in = data.checked_in;
				
				if($scope.selectedSession === null || typeof $scope.selectedSession === "undefined") {
					console.log($scope.view);
					switch($scope.view){
						case'checkin':
							attendeeElem.removeClass(attendeeObj.tile_view_state);
							attendeeElem.addClass('faded');	
							attendeeObj.tile_view_state='faded';
						break;

						case 'session':
							attendeeElem.removeClass(attendeeObj.tile_view_state);
							attendeeElem.addClass('active');					
							attendeeObj.tile_view_state='active';
							
						break;
					}
				}
				
			}else if(data.checkinType==='session'){
				console.log($scope.sessions);
				attendeeObj.att_session.push(data.session_id);
				
				var sessionAttendee = {
						"id": attendeeObj.attendee_id,
						"name": attendeeObj.attendee_name,
						"company_name": attendeeObj.attendee_company_name,
						"designation": attendeeObj.attendee_designation,
						"thumb_profile_pic": attendeeObj.attendee_photo_thumb
				};
				
				if($scope.selectedSession){
					
					if(data.session_id === $scope.selectedSession.session_id){
						$scope.selectedSession.session_attendees.push(sessionAttendee);
						console.log($scope.selectedSession);		
						switch($scope.view){
							case'checkin':
								attendeeElem.removeClass(attendeeObj.tile_view_state);
								attendeeElem.addClass('faded');	
								attendeeObj.tile_view_state='faded';
							break;

							case 'session':
								attendeeElem.removeClass(attendeeObj.tile_view_state);
								attendeeElem.addClass('active');	
								attendeeObj.tile_view_state='active';
							break;
						}
						console.log($scope.attendees[[data.attendee_id]]);
					}
				}
			}
		});
	};
	
	/***************** Get current event and call function to get all event attendees/sessions ***********************/
	$scope.eventData = activeEventService.getActiveEventData();
	if($scope.eventData){
		$scope.getAllEventAttendees($scope.eventData.id);
		$scope.pusherInit($scope.eventData.id);
	} else {
		alert('No active event is selected.');
		$window.location.href = '/';
	}
});
/**************************** END: checkinApp Controller ***********************************/