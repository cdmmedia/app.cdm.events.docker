/**************************** BEGIN: onsiteApp Controller ***********************************/
onsiteApp.controller('homeController', function ($scope, $http, activeEventService) 
{	
	/***************** Function to get all events *****************************/
	$scope.getAllEvents = function() {
		$http.get("//admin.dovetail.events/webservice/event/listEvents")
			.then(function(response) {	
				$scope.events = response.data;
				console.log($scope.events);
				$scope.getSelectedEvent();
			});
		
	};
	
	/***************** Function to set event id *****************************/
	$scope.setSelectedEvent = function(obj){
		activeEventService.setActiveEventData(obj);
	};
	
	/***************** Function to get event id *****************************/
	$scope.getSelectedEvent = function() { 
		var activeEvent = activeEventService.getActiveEventData();
		$scope.activeEvent = activeEvent;
		$scope.selectedOption=$scope.activeEvent;
	};
	$scope.getAllEvents();
	
});
/**************************** END: onsiteApp Controller ***********************************/
