onsiteApp.controller('meetingController', function ($scope, $http, $timeout, $interval, $uibModal, $log, $window, uiGridConstants, activeEventService) {
	$scope.myAppScopeProvider = {
		showInfo : function(row) {
		   var modalInstance = $uibModal.open({
				controller: 'InfoController',
				templateUrl: 'ngTemplate/infoPopup.html',
				resolve: {
				  selectedRow: function () {  
					  return row.entity;
				  }
				}
		   });
			modalInstance.result.then(function () {
				$log.log('Meeting updated successfully!');
			}, function () {
			 $log.info('Modal dismissed at: ' + new Date());
		   });
		},
		showDelegateInfo : function(row) {
		   var modalInstance = $uibModal.open({
				controller: 'InfoController',
				templateUrl: 'ngTemplate/infoDelegatePopup.html',
				resolve: {
				  selectedRow: function () {  
					  return row.entity;
				  }
				}
		   });
			modalInstance.result.then(function () {
				$log.log('Meeting updated successfully!');
			}, function () {
			 $log.info('Modal dismissed at: ' + new Date());
		   });
		}
	};
	
	
	
	$scope.timeSlotOpts = [{value: null, label:'All', selcted:'selected'}];
	$scope.sponsorCompanyOpts = [{value: null, label:'All', selcted:'selected'}];
	
	var timeSlotOptions = $scope.timeSlotOpts;
	var sponsorCompanyOptions = $scope.sponsorCompanyOpts;
	
	$scope.columns = [
		{ field: 'start_time', width: "35%", enableSorting: false, displayName: 'Time Slot' , filter: { term:null, type: uiGridConstants.filter.SELECT,selectOptions: timeSlotOptions}},
		{ field: 'company', width: "30%", displayName: 'Sponsor Company', filter: { term:null, type: uiGridConstants.filter.SELECT,selectOptions: sponsorCompanyOptions}},
		{ field: 'attendee_name', width: "30%", displayName: "Delegate Name",cellTemplate: '<div class="ui-grid-cell-contents" ng-click="grid.appScope.showDelegateInfo(row)">{{grid.getCellValue(row, col)}}</div>'},
		{ name: 'action', width: "5%", enableFiltering: false, type: 'boolean', displayName: 'X',cellTemplate: '<span class="glyphicon glyphicon-cog" ng-click="grid.appScope.showInfo(row)" ng-model="row.entity.action"></span>'} 
		
	];
	
	var rowtpl='<div ng-class="{\'green\':true, \'blue\':row.entity.conducted==1 }"><div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }" ui-grid-cell></div></div>';
	
	$scope.gridOptions = {
		columnDefs: $scope.columns,
		enableSorting: true,
		enableFiltering: true,
		enableColumnMenus: false,
		enableRowSelection: true,
		enableRowHeaderSelection:false,
		appScopeProvider: $scope.myAppScopeProvider,
		rowTemplate: rowtpl,
    };
	$scope.gridOptions.multiSelect = false;
	$scope.gridOptions.rowHeight = 50;

	$scope.getAllMeetings = function(id) {
		$http.get("https://admin.dovetail.events/webservice/oneOnOneMeetings/meetingsByEvent?event_id="+id)
		.then(function(response) {
			angular.forEach(response.data, function(value, key){
				value.start_time = $scope.DisplayCurrentTime(value.start_time);

				var index = -1;
				for(var ctr=0;ctr < $scope.timeSlotOpts.length; ctr++) {
					if($scope.timeSlotOpts[ctr].value === value.start_time) {
						index = ctr;
						break;
					}
				}
				if(index === -1){
					$scope.timeSlotOpts.push({value: value.start_time, label:value.start_time});
				}

				index = -1;
				for(ctr=0;ctr < $scope.sponsorCompanyOpts.length; ctr++) {
					if($scope.sponsorCompanyOpts[ctr].value === value.company) {
						index = ctr;
						break;
					}
				}
				if(index === -1){
					$scope.sponsorCompanyOpts.push({value: value.company, label:value.company});
				}

			});

			$scope.gridOptions.data = response.data;
		});
	};
	
	
	$scope.parsedDate = function(date) {
	  var parsed = Date.parse(date);
	  if (!isNaN(parsed)) {return parsed;} // this returns the date for most systems
	  return Date.parse(date.replace(/-/g, '/').replace(/[a-z]+/gi, ' ')); // this prepares the date specially for mac / safari
	}; 
	
	$scope.formattedDate = function(d) {
		var locale = "en-us";
		return d.toLocaleDateString(locale, { month: "short", day : '2-digit'});
		//return d;
	};
	
	$scope.DisplayCurrentTime = function(dateString) {
		
		var date = new Date($scope.parsedDate(dateString));
		var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
		var am_pm = date.getHours() >= 12 ? "PM" : "AM";
		var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

		var d = $scope.formattedDate(date);
		var time = d + " - " + hours + ":" + minutes + " " + am_pm;

		return time;
	};
	
	/***************** Get current event and call function to get all meetings ***********************/
	$scope.eventData = activeEventService.getActiveEventData();
	if($scope.eventData){
		$scope.getAllMeetings($scope.eventData.id);
	} else {
		alert('No active event is selected.');
		$window.location.href = '/';
	}	
});

onsiteApp.controller('InfoController', function ($scope, $http, $uibModal, $uibModalInstance, $filter, $interval, selectedRow) {
	$scope.selectedRow = selectedRow;

	$scope.updateConducted = function(){
		console.log(selectedRow.conducted);
		var dataObj = {
			meeting_id:selectedRow.id,
			conducted : +selectedRow.conducted
		}; 
		var url = 'https://admin.dovetail.events/webservice/oneOnOneMeetings/meetingConducted';
	
		var config = { headers : { 'Content-Type': 'application/json'}};
		var res = $http.post(url, dataObj, config);
	
		res.then(function(response){
			console.log(response);
			console.log(response.data);

		},function(response){
			console.log( "failure message:" + JSON.stringify({data: response}));
		});
	};

	$scope.cancel = function () {
		$scope.selectedRow = null;
		$uibModalInstance.dismiss('cancel');
	};
});