/***************************** Initiate onsiteApp module *************************************/
var onsiteApp = angular.module('onsiteApp', ['ngRoute', 'ngTouch', 'toggle-switch', 'ui.grid', 'ui.grid.selection', 'ui.grid.resizeColumns', 'ui.grid.edit', 'ui.bootstrap']);


/*********************************** BEGIN: route configuration ******************************/
onsiteApp.config(function($routeProvider, $locationProvider) {
	$routeProvider
		// route for the home page
		.when('/', {
			templateUrl : 'views/_home.html',
			controller  : 'homeController'
		})

		// route for the checkin page
		.when('/checkin', {
			templateUrl : 'views/_checkin.html',
			controller  : 'checkinController'
		})

		// route for the meeting page
		.when('/meeting', {
			templateUrl : 'views/_meeting.html',
			controller  : 'meetingController'
		});
	
	// use the HTML5 History API
	$locationProvider.html5Mode(true);
});
/*********************************** END: route configuration ******************************/

/***************************** BEGIN: onsiteApp service *************************************/
onsiteApp.service('activeEventService', function($http, $window) {
	var self = this;
	
	self.setActiveEventData = function (currentEvent) {
		$window.localStorage.setItem('eventData', JSON.stringify(currentEvent));
    };
	
    self.getActiveEventData = function () {
		var activeEvent = $window.localStorage.getItem('eventData');
		return JSON.parse(activeEvent);
    };
});
/*********************************** END: onsiteApp service ******************************/