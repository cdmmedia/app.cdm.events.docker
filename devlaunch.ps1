param([string]$port=80)

$repo = Split-Path $(Get-Location) -leaf
$localPath = $($(Get-Location).path -replace "\\", "/")
$container = "dev-" + $($repo -replace "\.","-" ) 
$appRoot = "/usr/share/nginx/html"
docker stop $container ; if($?) {docker rm $container -f}

$volumePaths = $localPath+"/code:"+$appRoot
$portBinding = $port+":80"
$(docker run -d --name $container -v $volumePaths -p $portBinding nginx:alpine)

docker exec -it $container sh -c $('cd '+ $appRoot + ' ; exec "${SHELL:-sh}"')
